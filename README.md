# SCALING ON AWS #

To run these pieces of code, you need to have the following packages installed: 


* [Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html), it provides functionalities such as create, configure and manage EC2 and S3 services of AWS to Python programmers.

* [Botocore](https://botocore.amazonaws.com/v1/documentation/api/latest/index.html), it is the foundation of AWS-CLI.

* [Selenium](https://pypi.org/project/selenium/), used to automate interactions between Python and web browsers.

